package ru.pcs.web.forms;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderForm {
    private Long clientId;
    private Long productId;
    private int count;
}
