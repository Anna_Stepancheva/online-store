package ru.pcs.web.forms;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductForm {
    private String name;
    private Integer count;
    private Double price;
}
