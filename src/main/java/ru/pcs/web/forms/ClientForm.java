package ru.pcs.web.forms;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientForm {

    private String firstName;
    private String secondName;
}
