package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Product;

import java.util.List;

@Component
public interface ProductRepository extends JpaRepository<Product, Long> {

}
