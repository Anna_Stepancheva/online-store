package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Order;

import java.util.List;

@Component
public interface OrdersRepository extends JpaRepository<Order, Long> {

List<Order> findAllByClientId(Long clientId);

}
