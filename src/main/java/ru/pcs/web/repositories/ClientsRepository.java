package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Client;


@Component
public interface ClientsRepository extends JpaRepository<Client, Long> {

}
