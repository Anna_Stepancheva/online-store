package ru.pcs.web.services.productService;

import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductService {
    void addProduct(ProductForm form);

    List<Product> findAllProducts();

    void deleteProduct(Long productId);

    Product getProduct(Long productId);

    void update(ProductForm form, Long productId);

    //List<Product> getAvailableProducts();
}
