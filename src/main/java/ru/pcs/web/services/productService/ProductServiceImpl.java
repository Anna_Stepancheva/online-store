package ru.pcs.web.services.productService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.OrdersRepository;
import ru.pcs.web.repositories.ProductRepository;

import java.util.List;


@Component
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    public final ProductRepository productRepository;
    public final OrdersRepository ordersRepository;


    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .name(form.getName())
                .count(form.getCount())
                .price(form.getPrice())
                .build();
        productRepository.save(product);
    }

    @Override
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public Product getProduct(Long productId) {
        return productRepository.getById(productId);
    }

    @Override
    public void update(ProductForm form, Long productId) {
        Product product = Product.builder()
                .id(productId)
                .name(form.getName())
                .count(form.getCount())
                .price(form.getPrice())
                .build();
        productRepository.save(product);
    }

//    @Override
//    public List<Product> getAvailableProducts() {
//        List<Product> products = productRepository.findAll();
//        return products;
//    }
}
