package ru.pcs.web.services.clientService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ClientsRepository;
import ru.pcs.web.repositories.OrdersRepository;
import ru.pcs.web.repositories.ProductRepository;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientsRepository clientsRepository;
    private final OrdersRepository ordersRepository;
    private final ProductRepository productRepository;

    @Override
    public void addClient(ClientForm form) {
        Client client = Client.builder()
                .firstName(form.getFirstName())
                .secondName(form.getSecondName())
                .build();

        clientsRepository.save(client);
    }

    @Override
    public List<Client> findAllClients() {
        return clientsRepository.findAll();
    }

    @Override
    public void deleteClient(Long clientId) {
        clientsRepository.deleteById(clientId);
    }

    @Override
    public Client getClient(Long clientId) {
        return clientsRepository.getById(clientId);
    }

    @Override
    public void update(ClientForm form, Long clientId) {
        Client client = Client.builder()
                .id(clientId)
                .firstName(form.getFirstName())
                .secondName(form.getSecondName())
                .build();
        clientsRepository.save(client);
    }

    @Override
    public List<Order> getOrdersByClient(Long clientId) {
        return ordersRepository.findAllByClientId(clientId);
    }


    @Override
    public void makeOrder(Long clientId, Long productId) {
        Client client = clientsRepository.getById(clientId);
        Product product = productRepository.getById(productId);
        product.setCount(product.getCount()-1);
        Order order = Order.builder()
                .product(product)
                .count(1)
                .client(client)
                .build();
        ordersRepository.save(order);
    }

}
