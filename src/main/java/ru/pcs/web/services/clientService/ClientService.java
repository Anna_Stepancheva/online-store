package ru.pcs.web.services.clientService;

import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ClientService {
    void addClient(ClientForm form);
    List<Client> findAllClients();
    void deleteClient(Long clientId);
    Client getClient(Long clientId);
    void update(ClientForm form, Long clientId);

    List<Order> getOrdersByClient(Long clientId);
    void makeOrder(Long clientId, Long productId);
}
