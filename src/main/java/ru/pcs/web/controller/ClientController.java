package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Order;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.clientService.ClientService;
import ru.pcs.web.services.orderService.OrderService;
import ru.pcs.web.services.productService.ProductService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;
    private final OrderService orderService;
    private final ProductService productService;

    @GetMapping("/clients")
    public String getClientsPage(Model model) {
        List<Client> clients = clientService.findAllClients();
        model.addAttribute("clients", clients);
        return "clients";
    }

    @GetMapping("/clients/{client-id}")
    public String getClientPage(Model model,
                                @PathVariable("client-id") Long clientId) {
        Client client = clientService.getClient(clientId);
        model.addAttribute("client", client);
        return "client";
    }

    @PostMapping("/clients")
    public String addClient(ClientForm form) {
        clientService.addClient(form);
        return "redirect:/clients";
    }

    @PostMapping("/clients/{client-id}/delete")
    public String deleteClient(@PathVariable("client-id") Long clientId) {
        clientService.deleteClient(clientId);
        return "redirect:/clients";
    }

    @PostMapping ("/clients/{client-id}/update")
    public String update(ClientForm form, @PathVariable("client-id") Long clientId) {
        clientService.update(form, clientId);
        return "redirect:/clients";
    }

    @GetMapping("/clients/{client-id}/orders")
    public String getOrdersByClient(Model model,
                                    @PathVariable("client-id") Long clientId){
        List<Order> orders = clientService.getOrdersByClient(clientId);
        List<Product> listProducts = productService.findAllProducts();
        model.addAttribute("clientId", clientId);
        model.addAttribute("listProducts", listProducts);
        model.addAttribute("orders", orders);
        return "orders_of_client";
    }

        @PostMapping("/clients/{client-id}/orders")
    public String addOrder(@PathVariable("client-id") Long clientId,
                                     @RequestParam("productId") Long productId){
        clientService.makeOrder(clientId, productId);
        return "redirect:/clients/" + clientId + "/orders";
    }
}
