package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.productService.ProductService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/products")
    public String getProductsPage (Model model) {
        List<Product> products = productService.findAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage (Model model,
                                  @PathVariable("product-id") Long productId) {
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm form) {
        productService.addProduct(form);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String update(ProductForm form, @PathVariable("product-id") Long productId){
        productService.update(form, productId);
        return "redirect:/products";
    }

}