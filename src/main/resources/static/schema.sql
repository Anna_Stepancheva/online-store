create table client
(
    id bigserial primary key,
    first_name varchar(20),
    second_name varchar(20)
);

insert into client (first_name, second_name)
VALUES ('Кирилл', 'Денисов'),
       ('Валентина', 'Иванова'),
       ('Дарья', 'Михайлова'),
       ('Алёна', 'Симонова'),
       ('Екатерина', 'Шишкина'),
       ('Савелий', 'Бродников'),
       ('София', 'Сидорова');

create table product
(
    id          bigserial primary key,
    name varchar(40),
    count       integer check (count >= 0),
    price double precision check (price > 0.0)
);

insert into product(name, count, price)
VALUES ('Вышивка крестом', 156, 1500),
       ('Картина по номерам', 225, 800.90),
       ('Алмазная картина', 623, 1357.30),
       ('Сборные модели', 108, 2300),
       ('Румбоксы', 99, 2500);

create table ordering
(
    id bigserial primary key,
    client_id bigint,
    product_id bigint,
    count integer,

    foreign key (client_id) references client (id),
    foreign key (product_id) references product(id)
);

insert into ordering (client_id, product_id, count)
VALUES (1, 4, 1),
       (2, 3, 2),
       (3, 1, 1),
       (6, 4, 1),
       (4, 5, 1),
       (7, 2, 1),
       (7, 3, 2);






